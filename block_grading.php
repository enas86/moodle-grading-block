<?php

class block_grading extends block_base
{
    public function init() {
        $this->title = get_string('grading', 'block_grading');
    }

    public function get_content() {
        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;

        $renderable = new \block_grading\output\main();
        $renderer = $this->page->get_renderer('block_grading');

        $this->content->text = $renderer->render($renderable);

        return $this->content;
    }
}