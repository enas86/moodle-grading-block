define(
[
    'jquery',
    'core/ajax',
    'core/templates',
    'core/notification'
],
function(
    $,
    ajax,
    templates,
    notification
) {
    var get_modules = function() {
        $('[data-region="main"] #allcourses').on('change', function () {
            var promises = ajax.call([{
                methodname: 'block_get_course_content_external',
                args: {
                    courseid: $('[data-region="main"] #allcourses').val(),
                    moduleid: -1,
                    userid: -1,
                }
            }]);
            promises[0].done(function (data) {
                templates.render("block_grading/main", data).done(function (html, js) {
                    $('select#modules').replaceWith($('select#modules', html));
                    $('select#students').replaceWith($('select#students', html));
                    $('p#currentmark').replaceWith($('p#currentmark', html));
                    $('p#minmax').replaceWith($('p#minmax', html));
                    $('div#assignparams').replaceWith($('div#assignparams', html));
                    //templates.runTemplateJS(js);
                }).fail(notification.exception);
            }).fail(notification.exception);
        });
    };

    var get_module_info = function () {
        $('[data-region="main"]').on('change', '#modules', function () {
            var promises = ajax.call([{
                methodname: 'block_get_course_content_external',
                args: {
                    courseid: $('[data-region="main"] #allcourses').val(),
                    moduleid: $('[data-region="main"] #modules').val(),
                    userid: -1
                }
            }]);
            promises[0].done(function (data) {
                templates.render("block_grading/main", data).done(function (html, js) {
                    $('select#students').replaceWith($('select#students', html));
                    $('p#currentmark').replaceWith($('p#currentmark', html));
                    $('p#minmax').replaceWith($('p#minmax', html));
                    $('div#assignparams').replaceWith($('div#assignparams', html));
                    //templates.runTemplateJS(js);
                }).fail(notification.exception);
            }).fail(notification.exception);
        });
    };

    var get_user_info = function () {
        $('[data-region="main"]').on('change', '#students', function () {
            var promises = ajax.call([{
                methodname: 'block_get_course_content_external',
                args: {
                    courseid: $('[data-region="main"] #allcourses').val(),
                    moduleid: $('[data-region="main"] #modules').val(),
                    userid: $('[data-region="main"] #students').val(),
                }
            }]);
            promises[0].done(function (data) {
                templates.render("block_grading/main", data).done(function (html, js) {
                    $('p#currentmark').replaceWith($('p#currentmark', html));
                    $('input#assignid').replaceWith($('input#assignid', html));
                    $('input#attemptnumber').replaceWith($('input#attemptnumber', html));
                    //templates.runTemplateJS(js);
                }).fail(notification.exception);
            }).fail(notification.exception);
        });
    };

    return {
        get_modules: get_modules,
        get_module_info: get_module_info,
        get_user_info: get_user_info
    };
});
