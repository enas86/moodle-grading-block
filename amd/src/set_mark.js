define(
    [
        'jquery',
        'core/ajax',
        'core/templates',
        'core/notification'
    ],
    function(
        $,
        ajax,
        templates,
        notification
    ) {
        /**
         * Save a student grade for a single assignment.
         *
         * @param int $assignmentid The id of the assignment
         * @param int $userid The id of the user
         * @param float $grade The grade (ignored if the assignment uses advanced grading)
         * @param int $attemptnumber The attempt number
         * @param bool $addattempt Allow another attempt
         * @param string $workflowstate New workflow state
         * @param bool $applytoall Apply the grade to all members of the group
         * @param array $plugindata Custom data used by plugins
         * @param array $advancedgradingdata Advanced grading data
         * @return null
         * @since Moodle 2.6
         */

        var set_mark = function () {
            $('[data-region="main"] #setmark').on('click', function () {
                var grade = Number($('input#grade').val());
                var grademin = Number($('input#grademin').val());
                var grademax = Number($('input#grademax').val());
                if(!Number.isNaN(grade) && !Number.isNaN(grademin) && !Number.isNaN(grademax))
                {
                    if(grade >= grademin && grade <= grademax)
                    {
                        var promises = ajax.call([{
                            methodname: 'block_set_mark_external',
                            args: {
                                assignmentid: $('input#assignid').val(),
                                userid: $('[data-region="main"] #students').val(),
                                grade: $('[data-region="main"] #grade').val(),
                                attemptnumber: $('[data-region="main"] #attemptnumber').val(),
                                addattempt: true,
                                workflowstate: "test",
                                applytoall: false,
                            }
                        }]);
                        promises[0].done(function (data) {
                            alert("Оценка изменена");
                        }).fail(notification.exception);
                    }
                    else alert("Некорректная оценка");
                }
                else alert("Некорректная оценка");
            });
        };

        return {
            set_mark: set_mark
        };
    });
