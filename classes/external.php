<?php

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/externallib.php");
require_once("$CFG->dirroot/webservice/externallib.php");
require_once("$CFG->dirroot/mod/assign/externallib.php");
//require_once("$CFG->dirroot/course/lib.php");
//require_once("$CFG->dirroot/course/externallib.php");

use external_api;
use external_function_parametrs;
use external_value;
use external_format_value;
use external_single_structure;
use external_multiple_structure;
use invalid_parameter_exception;
//use \core_course\external\course_summary_exporter;

class course_content extends external_api//core_course_external
{
    public static function get_course_content_parameters()
    {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'course id'),
                'moduleid' => new external_value(PARAM_INT, 'module id'),
                'userid' => new external_value(PARAM_INT, 'user id')
            )
        );
    }

    public static function get_course_content_is_allowed_from_ajax()
    {
        return true;
    }

    public static function get_course_content($courseid, $moduleid, $userid)
    {
        global $PAGE;

        $renderable = new \block_grading\output\main();
        $renderer = $PAGE->get_renderer('block_grading');
        return $renderable->export_for_template($renderer, $courseid, $moduleid, $userid);
    }
/*
    public static function get_course_contents($courseid, $options = array())
    {
        global $PAGE;
        GLOBAL $DB;

        $alltasks = $DB->get_records('assign', ['course' => 2]);

        $tasks = [];
        foreach ($alltasks as $assign)
        {
            $tasks[] = $assign;
        }

        $renderable = new \block_grading\output\main();
        $renderer = $PAGE->get_renderer('block_grading');
        return $renderable->export_for_template($renderer);


        return $tasks;
    }
*/
    public static function get_course_content_returns()
    {
        $result = new external_single_structure(
            array(
                'courses' => new external_multiple_structure(
                    new external_single_structure(
                        array('fullname' => new external_value(PARAM_RAW, 'course name'),
                              'id' => new external_value(PARAM_INT, 'course id')
                        )
                    )
                ),
                'modules' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'name' => new external_value(PARAM_RAW, 'activity module name'),
                            'cm' => new external_value(PARAM_INT, 'module id')
                        )
                    )
                ),
                'students' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'student id'),
                            'firstname' => new external_value(PARAM_RAW, 'users firstname'),
                            'lastname' => new external_value(PARAM_RAW, 'users lastname'),
                        )
                    )
                ),
                'grade' => new external_value(PARAM_RAW, 'users grade'),
                'assignid' => new external_value(PARAM_NUMBER, 'assign id'),
                'attemptnumber' => new external_value(PARAM_NUMBER, 'attempt number'),
                'grademax' => new external_value(PARAM_RAW, 'max grade'),
                'grademin' => new external_value(PARAM_RAW, 'min grade')
            )
        );

        return $result;
    }

    public static function save_grade_parameters()
    {
        return \mod_assign_external::save_grade_parameters();
    }

    public static function save_grade_is_allowed_from_ajax()
    {
        return true;
    }

    public static function save_grade($assignmentid, $userid, $grade, $attemptnumber, $addattempt, $workflowstate, $applytoall, $plugindata = array(), $advancedgradingdata = array())
    {
        return \mod_assign_external::save_grade($assignmentid, $userid, $grade, $attemptnumber, $addattempt, $workflowstate, $applytoall, $plugindata = array(), $advancedgradingdata = array());
    }

    public static function save_grade_returns()
    {
        return null;//\mod_assign_external::save_grade_returns();
    }
    /*
    public static function submit_grading_form_parameters()
    {
        return \mod_assign_external::submit_grading_form_parameters();
    }

    public static function submit_grading_form_is_allowed_from_ajax()
    {
        return true;
    }

    public static function submit_grading_form($assignmentid, $userid, $jsonformdata)
    {
        return \mod_assign_external::submit_grading_form($assignmentid, $userid, $jsonformdata);
    }

    public static function submit_grading_form_returns()
    {
        return \mod_assign_external::submit_grading_form_returns();
    }*/
}