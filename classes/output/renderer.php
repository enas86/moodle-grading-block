<?php

namespace block_grading\output;
defined('MOODLE_INTERNAL') || die();

use plugin_renderer_base;
use renderable;

class renderer extends plugin_renderer_base
{
    public function render_main(main $main)
    {
        return $this->render_from_template('block_grading/main', $main->export_for_template($this));
    }
}