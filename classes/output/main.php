<?php

namespace block_grading\output;
defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/externallib.php');
require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->dirroot . '/mod/assign/externallib.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/course/externallib.php');

use renderable;
use renderer_base;
use templatable;
use stdClass;
use assign;

class main implements renderable, templatable
{
    public function get_courses()
    {
        $mycourses = [];
        $courses = [];

        if ($mycourses = enrol_get_my_courses())
        {
            foreach ($mycourses as $course)
            {
                $coursecontext = \context_course::instance($course->id);
                if(has_capability('moodle/course:overridecompletion', $coursecontext))
                {
                    $courses[] = $course;
                }
            }
        }

        return $courses;
    }

    public function get_modules($courseid)
    {
        $coursemodules = [];
        $courseactivities = get_array_of_activities($courseid);

        foreach ($courseactivities as $courseactivity)
        {
            if($courseactivity->mod == "assign")
                $coursemodules[] = $courseactivity;
        }
        return $coursemodules;
    }

    public function get_students($courseid, $moduleid)
    {
        GLOBAL $USER;

        $result = null;
        $coursecontext = \context_course::instance($courseid);
        $modulecontext = \context_module::instance($moduleid);
        $students = get_enrolled_users($coursecontext);

        foreach ($students as $student)
            if($student->id != $USER->id && has_capability('mod/assign:submit', $modulecontext, $student->id)) $result[] = $student;
        return $result;
    }

    public function get_grade($courseid, $moduleid, $userid)
    {
        list ($course, $cm) = get_course_and_cm_from_cmid($moduleid);
        $coursemodulecontext = \context_module::instance($cm->id);

        $assign = new assign($coursemodulecontext, $cm, $course);

        $user = \core_user::get_user($userid, '*', MUST_EXIST);

        $previousattempts = $assign->get_assign_attempt_history_renderable($user);
        $assigninfo = $assign->get_grade_item();

        $result = null;

        $previousgrades = $previousattempts->grades;
        $attempt = array_pop($previousattempts->submissions);

        if($attempt == null)
        {
            $result['attemptnumber'] = 0;
            $result['grade'] = '-';
        }
        else
        {
            $result['attemptnumber'] = $attempt->attemptnumber;

            for($i = count($previousgrades); $i >= 0; $i--)
            {
                if(($temp = (array_pop($previousgrades))->grade) != -1)
                {
                    $result['grade'] = $temp;
                    break;
                }
            }

            if($result['grade'] == null) $result['grade'] = '-';
        }

        $result['assignid'] = $assigninfo->iteminstance;
        $result['grademax'] = $assigninfo->grademax;
        $result['grademin'] = $assigninfo->grademin;

        return $result;
    }

    public function export_for_template(renderer_base $output, $courseid = -1, $moduleid = -1, $userid = -1)
    {
        $courses = $modules = $students = $gradeinfo = null;
        $courses = $this->get_courses();

        if($courseid == -1)
        {
            if(count($courses) == 0) return null;
            else
            {
                $courseid = $courses[0]->id;
                $modules = $this->get_modules($courseid);
                $moduleid = $modules[0]->id;
            }
        }
        else
            $modules = $this->get_modules($courseid);

        if($moduleid == -1)
            $moduleid = $modules[0]->cm;

        $students = $this->get_students($courseid, $moduleid);
        if($userid == -1)
            $userid = $students[0]->id;

        $gradeinfo = $this->get_grade($courseid, $moduleid, $userid);

        $data = [
            'courses' => $courses,
            'modules' => $modules,
            'students' => $students,
            'grade' => $gradeinfo['grade'],
            'assignid' => $gradeinfo['assignid'],
            'attemptnumber' => $gradeinfo['attemptnumber'],
            'grademax' => $gradeinfo['grademax'],
            'grademin' => $gradeinfo['grademin']
        ];

        return $data;
    }
}