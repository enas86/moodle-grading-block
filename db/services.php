<?php

$functions = array(
    'block_get_course_content_external' => array( // local_PLUGINNAME_FUNCTIONNAME is the name of the web service function that the client will call.
        'classpath' => 'blocks/grading/classes/external.php',
        'classname'   => 'course_content', // create this class in componentdir/classes/external
        'methodname'  => 'get_course_content', // implement this function into the above class
        'description' => 'This documentation will be displayed in the generated API documentation 
                                          (Administration > Plugins > Webservices > API documentation)',
        'type'        => 'read', // the value is 'write' if your function does any database change, otherwise it is 'read'.
        'ajax'        => true, // true/false if you allow this web service function to be callable via ajax
        'capabilities'  => '',  // List the capabilities required by the function (those in a require_capability() call) (missing capabilities are displayed for authorised users and also for manually created tokens in the web interface, this is just informative).
    ),
    'block_set_mark_external' => array( // local_PLUGINNAME_FUNCTIONNAME is the name of the web service function that the client will call.
        'classpath' => 'blocks/grading/classes/external.php',
        'classname'   => 'course_content', // create this class in componentdir/classes/external
        'methodname'  => 'save_grade', // implement this function into the above class
        'description' => 'This documentation will be displayed in the generated API documentation 
                                          (Administration > Plugins > Webservices > API documentation)',
        'type'        => 'write', // the value is 'write' if your function does any database change, otherwise it is 'read'.
        'ajax'        => true, // true/false if you allow this web service function to be callable via ajax
        'capabilities'  => '',  // List the capabilities required by the function (those in a require_capability() call) (missing capabilities are displayed for authorised users and also for manually created tokens in the web interface, this is just informative).
    ),
);